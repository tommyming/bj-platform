import 'package:flutter/material.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';

class Search extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: SearchBar<Post>(onSearch: search, onItemFound: null),
          ),
        ),
      ),
    );
  }
}

class Post {
  String title = "";
  String description = "Please Enter Your Search";

  Post(this.title, this.description);
}

Future<List<Post>> search(String search) async {
  await Future.delayed(Duration(seconds: 2));
  return List.generate(search.length, (int index) {
    return Post(
      "Title : $search $index",
      "Description :$search $index",
    );
  });
}
