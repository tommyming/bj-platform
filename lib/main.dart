import 'package:bj_platform/search_widget.dart';
import 'package:flutter/material.dart';
import 'home_widget.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'navibar_widget.dart';

/*class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
} */

class MyApp extends StatelessWidget /*State<MyApp>*/ {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: IconButton(
          icon: Icon(Icons.menu),
          onPressed: null,
          disabledColor: Colors.red,
          hoverColor: Colors.grey,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.red),
            hoverColor: Colors.grey,
            // onPressed: () {
            // navigateToSearchPage(context);
            // },
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Search()),
              );
            },
          ),
        ],
      ),
      body: Home(),
      bottomNavigationBar: Container(
        child: BottomNavigationBar(
          // navigation bar for the whole application
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              backgroundColor: Colors.red,
              icon: Icon(Icons.home),
              title: Text("Home"),
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.red,
              icon: Icon(Icons.mood),
              title: Text("Liked"),
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.red,
              icon: Icon(Icons.search),
              title: Text("攻略"),
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.red,
              icon: Icon(Icons.chat_bubble_outline),
              title: Text("Chat"),
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.red,
              icon: Icon(Icons.person),
              title: Text("Personal"),
            )
          ],
          iconSize: 40,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.white30,
        ),
      ),
    );
  }
}

class Search extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SearchBar<Post>(onSearch: search, onItemFound: null),
        ),
      ),
    );
  }
}

void main() => runApp(MaterialApp(
      home: MyApp(),
    ));
