import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

Widget Home() {
  //Subject List for the carousel
  final List<String> subjectList = [
    "Chinese",
    // // "English
    "Maths",
    "Liberal Studies",
    "Econ",
    "BAFS",
    "Chem",
    "Bio",
    "Phy",
  ];

  //Ranking List for the carousel
  final List<String> rankingList = [
    "Beacon",
    "Modern Education",
    "Kings Education",
    "ABC English",
    "Testing Master"
  ];

  final List<String> privateList = [
    "Peter",
    "John",
    "Mary",
    "Jennifer",
    "Tommy",
    "Jane"
  ];
  //Item Slider preset for the Subject Carousel
  final List<Widget> subjectSlider = subjectList
      .map((item) => Container(
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Positioned(
                        child: Container(
                      decoration: BoxDecoration(color: Colors.redAccent),
                      child: Center(
                          child: Text(
                        "$item",
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold),
                      )),
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 20.0),
                    ))
                  ],
                ),
              ),
            ),
          ))
      .toList();

  final List<Widget> rankingSlider = rankingList
      .map((item) => Container(
              child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              child: Stack(
                children: <Widget>[
                  Positioned(
                      child: Container(
                    decoration: BoxDecoration(color: Colors.lightBlueAccent),
                    child: Center(
                        child: Text(
                      "$item",
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold),
                    )),
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                  ))
                ],
              ),
            ),
          )))
      .toList();
  final List<Widget> privateSlider = privateList
      .map((item) => Container(
              child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              child: Stack(
                children: <Widget>[
                  Positioned(
                      child: Container(
                    decoration: BoxDecoration(color: Colors.amberAccent),
                    child: Center(
                        child: Text(
                      "$item",
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold),
                    )),
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                  ))
                ],
              ),
            ),
          )))
      .toList();
  int current = 0;

  return SingleChildScrollView(
      child: Container(
    padding: const EdgeInsets.all(5.0),
    color: Colors.white,
    child: Container(
        child: Center(
            child: Column(children: [
      Padding(padding: EdgeInsets.symmetric(vertical: 10.0)),
      SizedBox(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
          child: Center(
            child: Text(
              'Announcement: This is a testing Announcement',
              style: new TextStyle(color: Colors.grey, fontSize: 15.0),
            ),
          ),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
              width: 3.0,
            ),
          ),
        ),
        width: double.infinity,
      ),
      SizedBox(height: 20),

      //Subject Choosing Column
      SizedBox(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Center(
              child: Row(
            children: [
              Text(
                'Peter, 你想補邊科?',
                style: TextStyle(color: Colors.grey, fontSize: 20.0),
              ),
            ],
          )),
          width: double.infinity,
        ),
      ),
      FlatButton(
        child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              "More >>",
            )),
        onPressed: () => print("go to subjects details page."),
      ),
      Column(
        children: [
          CarouselSlider(
            //This is the First Slideshow for Subjects
            options: CarouselOptions(
              height: 75.0,
              initialPage: 0,
              enableInfiniteScroll: true,
              reverse: false,
              autoPlay: true,
              enlargeCenterPage: true,
            ),
            items: subjectSlider,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: subjectList.map((i) {
              int index = subjectList.indexOf(i);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: current == index
                      ? Color.fromRGBO(0, 0, 0, 0.9)
                      : Color.fromRGBO(0, 0, 0, 0.4),
                ),
              );
            }).toList(),
          )
        ],
      ),

      //Best tutorial Centre Column
      SizedBox(height: 3),
      SizedBox(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Center(
              child: Row(
            children: [
              Text(
                '最佳補習社',
                style: TextStyle(color: Colors.grey, fontSize: 20.0),
              )
            ],
          )),
          width: double.infinity,
        ),
      ),
      FlatButton(
        child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              "More >>",
            )),
        onPressed: () => print("go to ranking details page."),
      ),
      CarouselSlider(
        //This is the Rank Slideshow
        options: CarouselOptions(
          height: 75.0,
          initialPage: 0,
          enableInfiniteScroll: false,
          reverse: false,
          autoPlay: false,
        ),
        items: rankingSlider,
      ),

      //info for private tutors
      SizedBox(height: 3),
      SizedBox(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Center(
              child: Row(
            children: [
              Text(
                '私補精選',
                style: TextStyle(color: Colors.grey, fontSize: 20.0),
              )
            ],
          )),
          width: double.infinity,
        ),
      ),
      FlatButton(
        child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              "More >>",
            )),
        onPressed: () => print("go to personal tutor details page."),
        color: Colors.white,
      ),
      CarouselSlider(
        //This is the Rank Slideshow
        options: CarouselOptions(
          height: 75.0,
          initialPage: 0,
          enableInfiniteScroll: false,
          reverse: false,
          autoPlay: false,
        ),
        items: privateSlider,
      ),

      //tutorial Centre Information
      SizedBox(height: 3),
      SizedBox(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Center(
              child: Row(
            children: [
              Text(
                '補習情報',
                style: TextStyle(color: Colors.grey, fontSize: 20.0),
              )
            ],
          )),
          width: double.infinity,
        ),
      ),
      FlatButton(
        child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              "More >>",
            )),
        onPressed: () => print("go to tutorial information details page."),
        color: Colors.white,
      ),
      SizedBox(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Center(
            child: Text(
              'Testing Placeholder',
              style: new TextStyle(color: Colors.grey, fontSize: 15.0),
            ),
          ),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
              width: 1.0,
            ),
          ),
        ),
        width: double.infinity,
      ),
    ]))),
  ));
}
