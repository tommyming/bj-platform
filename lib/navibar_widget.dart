import 'package:flutter/material.dart';
import 'main.dart';

Widget BottomBar()
{
  return Container
  (
    child: BottomNavigationBar(    // new
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          backgroundColor: Colors.red,
          icon: Icon(Icons.home),
          title: Text("Home"),
        ),
        BottomNavigationBarItem(
          backgroundColor: Colors.red,
          icon: Icon(Icons.mood),
          title: Text("Liked"),
        ),
        BottomNavigationBarItem(
          backgroundColor: Colors.red,
          icon: Icon(Icons.search),
          title: Text("攻略"),
          
        ),
        BottomNavigationBarItem(
          backgroundColor: Colors.red,
          icon: Icon(Icons.chat_bubble_outline),
          title: Text("Chat"),
        ),
        BottomNavigationBarItem(
          backgroundColor: Colors.red,
          icon: Icon(Icons.person),
          title: Text("Personal"),
        )
    ],
    iconSize: 40,
    selectedItemColor: Colors.white,
    unselectedItemColor: Colors.white30,
    ),
  );
}