import 'package:flutter/material.dart';


class TopBar extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return AppBar(
    backgroundColor: Colors.white,
    iconTheme: IconThemeData(
      color: Colors.red,),
    actionsIconTheme: IconThemeData(color: Colors.red),
    title: IconButton(
      icon: Icon(Icons.menu),
      onPressed: null,
      disabledColor: Colors.red,
      hoverColor: Colors.grey,
    ),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.search),
        // onPressed: () {
          // navigateToSearchPage(context);
        // },
        onPressed: null, 
        disabledColor: Colors.red,
        hoverColor: Colors.grey,
      ),
    ],
  );
  }

}

/*Future navigateToSearchPage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: BuildContext context) { return Search();})
} */